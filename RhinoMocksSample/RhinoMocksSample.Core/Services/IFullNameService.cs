﻿namespace RhinoMocksSample.Core.Services
{
    public interface IFullNameService
    {
        string FormatFullName(string firstName, string middleName, string lastName);
    }
}
