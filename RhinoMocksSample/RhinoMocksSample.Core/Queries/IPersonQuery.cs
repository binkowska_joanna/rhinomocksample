﻿using RhinoMocksSample.Core.Entities;

namespace RhinoMocksSample.Core.Queries
{
    public interface IPersonQuery
    {
        Person GetById(int id);
    }
}
