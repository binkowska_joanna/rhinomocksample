﻿using Microsoft.Practices.ServiceLocation;
using RhinoMocksSample.Services;
using System;

namespace RhinoMocksSample.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ContainerBootstrapper.ConfigureContainer();

            var fullNameService = ServiceLocator.Current.GetInstance<FullNameService>();
            var personFullName = fullNameService.FormatFullName(5);

            Console.WriteLine(personFullName);

            Console.ReadKey();
        }
    }
}
