﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using RhinoMocksSample.Core.Queries;
using RhinoMocksSample.Core.Services;
using RhinoMocksSample.Queries;
using RhinoMocksSample.Services;

namespace RhinoMocksSample.ConsoleApp
{
    public static class ContainerBootstrapper
    {
        internal static readonly IUnityContainer _container = new UnityContainer();

        internal static void ConfigureContainer()
        {
            ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(_container));
            //
            // More flexible - we can change IoC container without changing getting instance 
            // var logger = ServiceLocator.Current.GetInstance<ILogger>();

            // Created by default everytime they are requested
            _container.RegisterType<IFullNameService, FullNameService>();
            _container.RegisterType<IPersonQuery, PersonQuery>();
        }
    }
}
