﻿using RhinoMocksSample.Core.Queries;
using RhinoMocksSample.Core.Services;

namespace RhinoMocksSample.Services
{
    public class FullNameService : IFullNameService
    {
        private IPersonQuery _personQuery;
            
        public FullNameService(IPersonQuery personQuery)
        {
            _personQuery = personQuery;
        }

        /// <summary>
        /// Ex. 1
        /// W projekcie RhinoMocksSample.Tests.Unit dodajcie metodę testującą
        /// Wykorzystajcie atrybut NUnit [TestCase]
        /// Wg konwencji Services/FullNameServiceTests/FormatFullName_...
        /// </summary>
        public string FormatFullName(string firstName, string middleName, string lastName)
        {
            if (string.IsNullOrEmpty(firstName) 
                || string.IsNullOrEmpty(lastName))
                return null;

            if (string.IsNullOrEmpty(middleName))
                return string.Format("{0} {1}", firstName, lastName);

            return string.Format("{0} {1} {2}", 
                firstName, 
                middleName,
                lastName);
        }

        /// <summary>
        /// Ex. 2
        /// W projekcie RhinoMocksSample.Tests.Unit dodajcie metodę testującą
        /// Wykorzystajcie atrybut NUnit [TestCase] oraz Rhino Mocks
        /// Wg konwencji Services/FullNameServiceTests/FormatFullName_...
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string FormatFullName(int id)
        {
            var person = _personQuery.GetById(id);

            if (string.IsNullOrEmpty(person.FirstName)
                || string.IsNullOrEmpty(person.LastName))
                return null;

            if (string.IsNullOrEmpty(person.MiddleName))
                return string.Format("{0} {1}", person.FirstName, person.LastName);

            return string.Format("{0} {1} {2}",
                person.FirstName,
                person.MiddleName,
                person.LastName);
        }
    }
}
