﻿using RhinoMocksSample.Core.Queries;
using System.Collections.Generic;
using System.Linq;
using RhinoMocksSample.Core.Entities;

namespace RhinoMocksSample.Queries
{
    public class PersonQuery : IPersonQuery
    {
        public Person GetById(int id)
        {
            return Persons.FirstOrDefault(p => p.Id == id);
        }

        public List<Person> Persons
        {
            get
            {
                return new List<Person>
                {
                    new Person { Id = 1, FirstName = "Jan", LastName = "Kowalski", MiddleName = "Paweł" },
                    new Person { Id = 2, FirstName = "Kamil", LastName = "Nowak", MiddleName = "Paweł" },
                    new Person { Id = 3, FirstName = "Agnieszka", LastName = "Testowa" },
                    new Person { Id = 4, FirstName = "Michał", LastName = "Brzęczyszczykiewicz", MiddleName = "Aleksander" },
                    new Person { Id = 5, FirstName = "Joanna", LastName = "Nowak" },
                    new Person { Id = 6, FirstName = "Anna", LastName = "Pawlikowska-Jankowska", MiddleName = "Beata" },
                    new Person { Id = 7, FirstName = "Dominika", LastName = "Zdort", MiddleName = "Milena" },
                    new Person { Id = 8, FirstName = "Michalina", LastName = "Wojciechowska" },
                    new Person { Id = 9, FirstName = "Adam", LastName = "Robak" },
                    new Person { Id = 10, FirstName = "Kamila", LastName = "Wośkowska", MiddleName = "Małgorzata" },
                };
            }
        }
    }
}
